import { RESTDataSource } from "apollo-datasource-rest";
import server from "../server.js";

class AuthAPI extends RESTDataSource {

        constructor() {
            super();
            this.baseURL = server.auth_api_url;
        }
        async users(){
            return await this.get('users');
        }
        async userById(id){
            return await this.get(`users/${id}`);
        }
    
        async newUser(body) {
            return await this.post('users', body);
        }
        async deleteUser(id) {
            return await this.delete(`users/${id}`);
        }
        async login(credentials) {
            credentials= new Object(JSON.parse(JSON.stringify(credentials)));
            return await this.post('login', credentials);
        }
        async refreshToken(token){
            token= new Object(JSON.parse(JSON.stringify({refresh: token})));
            return await this.post(`token/refresh`,token);
        }
    
    }

export default AuthAPI