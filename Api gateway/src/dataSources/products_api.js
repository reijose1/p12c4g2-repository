import { RESTDataSource } from "apollo-datasource-rest";
import server from "../server.js";

class ProductAPI extends RESTDataSource {

    constructor(){
        super()
        this.baseURL = server.products_api_url;
    }
    async products(){
        return await this.get('/products/products');
    }
    async productById(id){
        return await this.get(`/products/products/${id}`);
    }
    async newProduct(body) {
        return await this.post(`/products/products`, body);
    }
    async updateProduct(data) {
        const { id, ...body } = data;
        return await this.put(`/products/products/${id}`, body);
    }
    async deleteProduct(id){
        return await this.delete(`/products/products/${id}`);
    }
    async categories(){
        return await this.get('products/category-products/')
    }
    async categoryById(id){
        return await this.get(`/products/category-products/${id}`);
    }
    async newCategory(body) {
        return await this.post(`products/category-products/`, body);
    }
    async updateCategory(data) {
        const { id, ...body } = data;
        return await this.put(`products/category-products/${id}`, body);
    }
    async deleteCategory(id){
        return await this.delete(`/products/category-products/${id}`);
    }
    async productByCategory(){
        return await this.post('/products/products');
    }
}

export default ProductAPI;