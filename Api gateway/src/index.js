import { ApolloServer } from "apollo-server"; 
import typeDefs from "./typeDefs/index.js";
import resolvers from "./resolvers/index.js";
import ProductAPI from "./dataSources/products_api.js";
import AuthAPI from "./dataSources/auth_api.js";

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({
        productAPI: new ProductAPI(),
        authAPI: new AuthAPI(),
    }),
    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then(({ url }) => {
    console.log(`Servidor corriendo en ${url}`)
});