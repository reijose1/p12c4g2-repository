const authResolver = {

    Query: {
        users: async (root, args,{dataSources}) => {
            return await dataSources.authAPI.users();
        },
        userById: async (root, { id },{dataSources}) => {
            return await dataSources.authAPI.userById(id);
        },
    },

    Mutation: {
        login: (_,{credetials},{dataSources}) =>
        dataSources.authAPI.login(credetials),

        refreshToken: (_, {refresh},{dataSources})=>
        dataSources.authAPI.refreshToken(refresh),
    }
}

export default authResolver;