const categoryResolver = {

    Query: {
        categories: async (root,args,{dataSources}) => {
            return await dataSources.productAPI.categories();
        },
        categoryById: async (root, { id },{dataSources}) => {
            return await dataSources.productAPI.categoryById(id);
        }
        
    },

    Mutation: {
        newCategory: async(root, args, { dataSources }) => {
            return await dataSources.productAPI.newCategory(args);
        },
        updateCategory: async(root, args, { dataSources }) => {
            return await dataSources.productAPI.updateCategory(args)
        },
        deleteCategory: async(root, {id}, { dataSources }) => {
            return await dataSources.productAPI.deleteCategory(id)
        }
    
    }
}

export default categoryResolver;