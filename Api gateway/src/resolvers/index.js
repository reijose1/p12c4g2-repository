import lodash from "lodash";
import categoryResolver from "./category_resolver.js";
import productResolver from "./products_resolvers.js";
import authResolver from "./auth_resolvers.js";

const resolvers = lodash.merge(productResolver, categoryResolver,authResolver);

export default resolvers ;