const productResolver = {

    Query:{
        products: async (root, args,{dataSources}) => {
            return await dataSources.productAPI.products();
        },
        productById: async (root, { id },{dataSources}) => {
            return await dataSources.productAPI.productById(id);
        }
        },

    Mutation: {
        newProduct: async(root, args, { dataSources }) => {
            return await dataSources.productAPI.newProduct(args);
        },
        updateProduct: async(root, args, { dataSources }) => {
            return await dataSources.productAPI.updateProduct(args)
        },
        deleteProduct: async(root, {id}, { dataSources }) => {
            return await dataSources.productAPI.deleteProduct(id)
        },
        productByCategory: async (root, {category_product},{dataSources}) => {

            const products = await dataSources.productAPI.products();
            const productByCategory = products.filter(function(element){
                return element.category_product == category_product;
            });
             return productByCategory;
}
}
}

export default productResolver;