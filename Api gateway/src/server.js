const server = {
    products_api_url: 'https://doker-product-p12c4g2.herokuapp.com',
    auth_api_url: 'https://doker-auth-p12c4g2.herokuapp.com'
}

export default server;