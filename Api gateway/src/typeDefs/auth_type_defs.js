import { gql } from "apollo-server";

const authTypeDefs = gql `

type  user {
    id: Int!
    name: String!
    username: String!
    email: String!
    password: String!
    is_active: Boolean!
    is_staff: Boolean!
}
type Tokens {
    refresh: String!
}

input CredentialsInput{
    username:String!
    password:String!
}

type Query {
    users: [user]
    userById(id: Int!): user
}

type Mutation {
    newUser(
    id: Int!
    name: String!
    username: String!
    email: String!
    password: String!
    is_active: Boolean!
    is_staff: Boolean!
    ): user
    login(credentials: CredentialsInput!): Tokens!
    refreshToken(refresh: String!): Tokens!
}
`

export default authTypeDefs