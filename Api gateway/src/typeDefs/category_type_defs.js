import { gql } from "apollo-server";

const categoryTypeDefs = gql `

    type category {
        id: Int!
        description: String!
    }

    extend type Query {
        categories: [category]
        categoryById(id: Int!): category
    
    }

    extend type Mutation {
        newCategory(
            id: Int!
            description: String!
        ): category

        updateCategory(
            id: Int!
            description: String!
        ): category

        deleteCategory(id:Int!): category
    }

`
export default categoryTypeDefs