import productsTypeDefs from "./products_type_defs.js";
import categoryTypeDefs from "./category_type_defs.js";
import authTypeDefs from "./auth_type_defs.js";

const typeDefs = [productsTypeDefs,categoryTypeDefs,authTypeDefs];

export default typeDefs;
