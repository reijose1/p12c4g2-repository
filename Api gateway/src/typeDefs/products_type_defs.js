import { gql } from "apollo-server";

const productsTypeDefs = gql `

    type product {
        id: Int!
        name: String!
        description: String!
        image: String!
        measure_unit: String!
        category_product: String!
        price: Int!
        stock: Int!
    }

    type Query {
        products: [product]
        productById(id: Int!): product
    }


    type Mutation {
        newProduct(
            id: Int!
            name: String!
            description: String!
            image: String!
            measure_unit: String!
            category_product: String!
            price: Int!
            stock: Int!
        ): product

        updateProduct(
            id: Int!
            name: String!
            description: String!
            image: String!
            measure_unit: String!
            category_product: String!
            price: Int!
            stock: Int!
        ): product

        deleteProduct(id:Int!): product

        productByCategory(category_product: String!): [product]!
    }
`
export default productsTypeDefs;