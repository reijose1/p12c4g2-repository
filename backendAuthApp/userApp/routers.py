from rest_framework.routers import DefaultRouter
from userApp.views import UserViewSet

router = DefaultRouter()

router.register('', UserViewSet, basename="users")

urlpatterns = router.urls