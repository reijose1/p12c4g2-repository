from django.contrib import admin
from productApp.models.general_models import *
from productApp.models.product_models import Product
# Register your models here.
class MeasureUnitAdmin(admin.ModelAdmin):
    list_display = ('id','description')

class CategoryProductAdmin(admin.ModelAdmin):
    list_display = ('id','description')

admin.site.register(MeasureUnit,MeasureUnitAdmin)
admin.site.register(CategoryProduct,CategoryProductAdmin)
admin.site.register(Indicator)
admin.site.register(Product)

