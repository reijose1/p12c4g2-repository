from django.db import models
from productApp.models.general_models import BaseModel
from productApp.models.general_models import MeasureUnit, CategoryProduct

class Product(BaseModel):
    """Model definition for Product."""

    # TODO: Define fields here
    name = models.CharField('Nombre de Producto', max_length=150, unique=True, blank=False, null=False)
    description = models.TextField('Descripción de Producto', blank=False, null=False)
    image = models.CharField('Imagen del Producto', max_length=513, blank=False, null=False)
    measure_unit = models.ForeignKey(MeasureUnit, on_delete=models.CASCADE, verbose_name='Unidad de Medida', null=True)
    category_product = models.ForeignKey(CategoryProduct, on_delete=models.CASCADE, verbose_name='Categoria de Producto', null=True)
    price = models.DecimalField('Precio', max_digits=8, decimal_places=2, default=0.0, blank=False, null=False)
    stock = models.IntegerField('Stock de Producto', default=0, blank=False, null=False)

    class Meta:
        """Meta definition for Product."""

        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        """Unicode representation of Product."""
        return self.name
