from rest_framework.routers import DefaultRouter
from productApp.views.general_views import *
from productApp.views.product_views import ProductViewSet

router = DefaultRouter()

router.register(r'products',ProductViewSet,basename = 'products')
router.register(r'measure-unit',MeasureUnitViewSet,basename = 'measure_unit')
router.register(r'indicators',IndicatorViewSet,basename = 'indicators')
router.register(r'category-products',CategoryProductViewSet,basename = 'category_products')

urlpatterns = router.urls