from rest_framework import serializers
from productApp.models.general_models import MeasureUnit, CategoryProduct
from productApp.models.product_models import Product
from productApp.serializers.general_serializers import MeasureUnitSerializer,CategoryProductSerializer

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        exclude = ('state','created_date','modified_date','deleted_date')

    def validate_measure_unit(self, value):
        if value == '' or value == None:
            raise serializers.ValidationError("Debe ingresar una Unidad de Medida.")
        return value

    def validate_category_product(self, value):
        if value == '' or value == None:
            raise serializers.ValidationError("Debe ingresar una Categoría de Producto")
        return value

    def validate(self, data):
        if 'measure_unit' not in data.keys():
            raise serializers.ValidationError({
                "measure_unit": "Debe ingresar una Unidad de Medida."
            })
        if 'category_product' not in data.keys():
            raise serializers.ValidationError({
                "category_product": "Debe ingresar una Categoria de Producto."
            })
        return data

    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'image': instance.image,
            'measure_unit': instance.measure_unit.description if instance.measure_unit is not None else '',
            'category_product': instance.category_product.description if instance.category_product is not None else '',
            'price' : instance.price,
            'stock' : instance.stock
        }
