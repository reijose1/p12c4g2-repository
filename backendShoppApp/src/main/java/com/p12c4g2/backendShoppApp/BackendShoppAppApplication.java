package com.p12c4g2.backendShoppApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendShoppAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendShoppAppApplication.class, args);
	}

}
