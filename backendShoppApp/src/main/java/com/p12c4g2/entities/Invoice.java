package com.p12c4g2.entities;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Invoice {

	@Id
	private Integer id;
	private String numberInvoice;
	private Integer customerId;
	@JsonFormat
	@CreatedDate
	private Date createAt;
	private List<InvoiceItem> items;
	private Double totalCompra;
	
	public Invoice() {
		super();
	}

	public Invoice(Integer id, String numberInvoice, Integer customerId, Date createAt, List<InvoiceItem> items,
			Double totalCompra) {
		super();
		this.id = id;
		this.numberInvoice = numberInvoice;
		this.customerId = customerId;
		this.createAt = createAt;
		this.items = items;
		this.totalCompra = totalCompra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumberInvoice() {
		return numberInvoice;
	}

	public void setNumberInvoice(String numberInvoice) {
		this.numberInvoice = numberInvoice;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public List<InvoiceItem> getItems() {
		return items;
	}

	public void setItems(List<InvoiceItem> items) {
		this.items = items;
	}

	public Double getTotalCompra() {
		return totalCompra;
	}

	public void setTotalCompra(Double totalCompra) {
		this.totalCompra = totalCompra;
	}

	
	
}
