package com.p12c4g2.entities;

import org.springframework.data.annotation.Id;


public class InvoiceItem {

		@Id
		private Integer id;
		private String numberInvoice;
		private Integer productId;
		private Double quantity;
		private Double subTotal;
		
		public InvoiceItem(Integer id, String numberInvoice, Integer productId, Double quantity, Double subTotal) {
			super();
			this.id = id;
			this.numberInvoice = numberInvoice;
			this.productId = productId;
			this.quantity = quantity;
			this.subTotal = subTotal;
		}
		public InvoiceItem() {
			super();
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getNumberInvoice() {
			return numberInvoice;
		}
		public void setNumberInvoice(String numberInvoice) {
			this.numberInvoice = numberInvoice;
		}
		public Integer getProductId() {
			return productId;
		}
		public void setProductId(Integer productId) {
			this.productId = productId;
		}
		public Double getQuantity() {
			return quantity;
		}
		public void setQuantity(Double quantity) {
			this.quantity = quantity;
		}
		public Double getSubTotal() {
			return subTotal;
		}
		public void setSubTotal(Double subTotal) {
			this.subTotal = subTotal;
		}
		
		
		
	}

